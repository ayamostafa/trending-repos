import "./App.css";
import Repos from "./containers/Repos/Repos";

function App() {
  return (
    <div className="App">
      <Repos />
    </div>
  );
}

export default App;
