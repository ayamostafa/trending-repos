import React, { Component } from "react";
import axios from "axios";

import Repository from "../../components/Repository";
class Repos extends Component {
  state = {
    repos: [],
  };
  componentDidMount() {
    axios
      .get(
        "https://api.github.com/search/repositories?q=created:%3E2017-10-22&sort=stars&order=desc"
      )
      .then((response) => {
        console.log(response.data.items.slice(0, 4));
        this.setState({ repos: response.data.items.slice(0, 4) });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    let repos = this.state.repos;
    repos = repos.map((repo) => {
      return (
        <Repository
          key={repo.id}
          owner_avatar={repo.owner.avatar_url}
          name={repo.name}
          desc={repo.description}
          num_stars={repo.stargazers_count}
          num_issues={repo.open_issues_count}
          last_updated={repo.updated_at}
        />
      );
    });
    return <div>{repos}</div>;
  }
}
export default Repos;
